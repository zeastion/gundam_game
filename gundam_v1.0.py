#!/usr/bin/python
# -*- coding:utf-8 -*-
# GUNDAM Python Game V1.0 Design

from sys import exit
import time,os,sys,tty,termios

def state():

    global ak, df, nak, ndf
    print
    print

    if NUMBER == "1":
        print "+------------------------------+"
        print "| 1- GAT-X105 Strike Gundam    |"
        print "--------------------------------"
        print "|    HP                    550 |"
        print "--------------------------------"
        print "|    MP                    150 |"
        print "--------------------------------"
        print "|    攻击力     75 + %d = %d |" % (nak, ATK)
        print "--------------------------------"
        print "|    防御力     30 + %d = %d |" % (ndf, DEF)
        print "+------------------------------+"

    elif NUMBER == "2":
        print "+------------------------------+"
        print "| 2- MSZ-008 ZII GUNDAM ZII    |"
        print "--------------------------------"
        print "|    HP                    450 |"
        print "--------------------------------"
        print "|    MP                    200 |"
        print "--------------------------------"
        print "|    攻击力     65 + %d = %d |" % (nak, ATK)
        print "--------------------------------"
        print "|    防御力     45 + %d = %d |" % (ndf, DEF)
        print "+------------------------------+"

    elif NUMBER == "3":
        print "+------------------------------+"
        print "| 3- ZM-S08G ZOLO              |"
        print "--------------------------------"
        print "|    HP                    500 |"
        print "--------------------------------"
        print "|    MP                    180 |"
        print "--------------------------------"
        print "|    攻击力     70 + %d = %d |" % (nak, ATK)
        print "--------------------------------"
        print "|    防御力     35 + %d = %d |" % (ndf, DEF)
        print "+------------------------------+"

    else:
        print "+------------------------------+"
        print "| 4- GN-001 Gundam Exia        |"
        print "--------------------------------"
        print "|    HP                    600 |"
        print "--------------------------------"
        print "|    MP                    200 |"
        print "--------------------------------"
        print "|    攻击力     80 + %d = %d |" % (nak, ATK)
        print "--------------------------------"
        print "|    防御力     35 + %d = %d |" % (ndf, DEF)
        print "+------------------------------+"

    print
    time.sleep(1)
    print "   Jack >>> 勇士 一切妥当了 开始你的战斗年华吧 绽放啊少年！！\n"
    time.sleep(1)
    print "   Zeation >>> ......\n"
    time.sleep(1)
    print "   作 者 >>> ......\n"
    time.sleep(1)

    raw_input("\n前往战场 ...")

    exit(0)

def completed():

    global ak, df, nak, ndf, ATK, DEF  # 有的变量不需要  下个版本删除

    ATK = ak + nak
    DEF = df + ndf

    time.sleep(0.5)
    print "\n\n   武器安装完成 ------------------- 100%\n"
    time.sleep(0.5)
    print "   防具装配完成 ------------------- 100%\n"
    passw = raw_input("\n输入GundamOS密码查看高达第二阶段状态：")

    if passw == PASSWORD:
        
        print "\n密码正确！"
        print "\n第二阶段展开 ...\n"
        time.sleep(1)
        state()

    else:

        over("密码错误！")




#    print "基础攻击力 %d + 武器攻击附加 %d ---> 完全攻击力 %d" % (ak, nak, ATK)
#    print
#    print "基础防御力 %d + 防具防御附加 %d ---> 完全防御力 %d" % (df, ndf, DEF)

   

def drive():
  
    # 绑定机体和驾驶员    

    information()
    print "|    驾驶员          %s |" % name
    print "-----------------------------"

def equiparmor():

    global GOLD, ndf
    armor = raw_input("输入需要购买的装备编号[1-2] 或输入 \'n\' 放弃购买 : ")
    print
    
    if armor == "1":
        print "获得防具：原子盾[白]"
        
        drive()
        print "| 原子盾[白]   >>>   已装备 |"
        print "+---------------------------+\n" 
        GOLD = GOLD - 600
        print "\n资    金：-600\n"
        print "账户余额：%d\n" % GOLD   
        raw_input("绑定完毕 防御力 + 90 \n\n敲击回车确定")
        ndf = 90
        print

    elif armor == "2":
        print "获得武器：纳米光罩[蓝]"
        
        drive()
        print "| 纳米光罩[蓝] >>>   已装备 |"
        print "+---------------------------+\n"  
        print "\n资    金：-700\n"
        GOLD = GOLD - 700
        print "账户余额：%d\n" % GOLD  
        raw_input("绑定完毕 防御力 + 120 \n\n敲击回车确定")
        ndf = 120
        print
    
    elif armor == "n":
        print "啥也没买 抠死了 = . =\n"
        ndf = 0
        print "账户余额：%d\n" % GOLD

    else:
        over("不买就不买 还乱敲键盘 ")

    print "\n   Jack >>> 是否购买武器？\n"
    print "   1- 买！"
    print "   2- 不买了..."
    print

    con = raw_input("请选择[1-2] ： ")
    print
    
    if con == "1":
        wea()
    elif con == "2":
        completed()


def equipwea():
   
    global GOLD, nak
    weapon = raw_input("输入需要购买的装备编号[1-3] 或输入 \'n\' 放弃购买 : ")
    print

    if weapon == "1":
        print "获得武器：光束枪[白]"
        
        drive()
        print "| 光束枪[白]   >>>   已装备 |"
        print "+---------------------------+\n" 
        print "\n资    金：-700\n"
        GOLD = GOLD - 700
        print "账户余额：%d\n" % GOLD   
        raw_input("绑定完毕 攻击力 + 120 \n\n敲击回车确定")
        nak = 120
        print

    elif weapon == "2":
        print "获得武器：脉冲巨剑[蓝]"
        
        drive()
        print "| 脉冲巨剑[蓝] >>>   已装备 |"
        print "+---------------------------+\n"
        print "\n资    金：-800\n"
        GOLD = GOLD - 800
        print "账户余额：%d\n" % GOLD    
        raw_input("绑定完毕 攻击力 + 150 \n\n敲击回车确定")
        nak =150
        print

    elif weapon == "3":	
        print "获得武器：强袭战戟[橙]"
        
        drive()
        print "| 强袭战戟[橙] >>>   已装备 |"
        print "+---------------------------+\n"
        print "\n资    金：-1000\n"
        GOLD = GOLD - 1000
        print "账户余额：%d\n" % GOLD    
        raw_input("绑定完毕 攻击力 + 210 \n\n敲击回车确定")
        nak = 210
        print

    elif weapon == "n":
        print "啥也没买 抠死了 = . =\n"
        nak = 0
        print "账户余额：%d\n" % GOLD

    else:
        over("不买就不买 还乱敲键盘 ")

    print "\n   Jack >>> 是否购买防具？\n"
    print "   1- 买！"
    print "   2- 不买了..."
    print

    con = raw_input("请选择[1-2] ： ")
    print
    
    if con == "1":
        armor()
    elif con == "2":
        completed()
        

def wea():
    print "    ***武器库开启中***\n"
    time.sleep(0.5)
     
    print "+---------------------------+"
    print "| 1- 光束枪[白]             |"
    print "-----------------------------"
    print "|    攻击力             120 |"
    print "-----------------------------"
    print "|    价  格             700 |"
    print "+---------------------------+"
    print "|                           |"
    print "+---------------------------+"
    print "| 2- 脉冲巨剑[蓝]           |"
    print "-----------------------------"
    print "|    攻击力             150 |"
    print "-----------------------------"
    print "|    价  格             800 |"
    print "+---------------------------+"
    print "|                           |"
    print "+---------------------------+"
    print "| 3- 强袭战戟[橙]           |"
    print "-----------------------------"
    print "|    攻击力             210 |"
    print "-----------------------------"
    print "|    价  格            1000 |"
    print "+---------------------------+"
    print

    equipwea() 

def armor():
    print "   ***防具库开启中***\n"
    time.sleep(0.5)  
  
    print "+---------------------------+"
    print "| 1- 原子盾[白]             |"
    print "-----------------------------"
    print "|    防御力              90 |"
    print "-----------------------------"
    print "|    价  格             600 |"
    print "+---------------------------+"
    print "|                           |"
    print "+---------------------------+"
    print "| 2- 纳米光罩[蓝]           |"
    print "-----------------------------"
    print "|    防御力             120 |"
    print "-----------------------------"
    print "|    价  格             700 |"
    print "+---------------------------+"
    print

    equiparmor()

def equip():

    print "\n  Jack >>> 你好 勇士 \n  Jack >>> 我是这家店的老板 我们提供 \033[1;31;40m武器\033[0m 和 \033[1;31;40m防具\033[0m 你需要哪一个？\n"
    print "系统提示： 武器和防具都只能装备一件 请仔细挑选\n"
    need = raw_input("我需要 : ")
    print

    if "武器" in need:
        wea()

    elif "防具" in need:
        armor()


def over(why):
    
    print why, "对不起 \033[5;31;47m%s\033[0m 你不适合做高达驾驶员" % name
    print "\n\n    *** GAME OVER ***"
    exit(0)

def information():

    global NUMBER, hp, mp, ak, df   
    if NUMBER == "1":
        print "+---------------------------+"
        print "| 1- GAT-X105 Strike Gundam |"
        print "-----------------------------"
        print "|    初始HP             550 |"
        print "-----------------------------"
        print "|    初始MP             150 |"
        print "-----------------------------"
        print "|    攻击力              75 |"
        print "-----------------------------"
        print "|    防御力              30 |"
        print "+---------------------------+"

        # 高达属性赋值

        hp = 550
        mp = 150
        ak = 75
        df = 30

    elif NUMBER == "2":
        print "+---------------------------+"
        print "| 2- MSZ-008 ZII GUNDAM ZII |"
        print "-----------------------------"
        print "|    初始HP             450 |"
        print "-----------------------------"
        print "|    初始MP             200 |"
        print "-----------------------------"
        print "|    攻击力              65 |"
        print "-----------------------------"
        print "|    防御力              45 |"
        print "+---------------------------+"
 
        hp = 450
        mp = 200
        ak = 65
        df = 45

    elif NUMBER == "3":
        print "+---------------------------+"
        print "| 3- ZM-S08G ZOLO           |"
        print "-----------------------------"
        print "|    初始HP             500 |"
        print "-----------------------------"
        print "|    初始MP             180 |"
        print "-----------------------------"
        print "|    攻击力              70 |"
        print "-----------------------------"
        print "|    防御力              35 |"
        print "+---------------------------+"

        hp = 500
        mp = 180
        ak = 70
        df = 35

    elif NUMBER == "4":
        print "+---------------------------+"
        print "| 4- GN-001 Gundam Exia     |"
        print "-----------------------------"
        print "|    初始HP             600 |"
        print "-----------------------------"
        print "|    初始MP             200 |"
        print "-----------------------------"
        print "|    攻击力              80 |"
        print "-----------------------------"
        print "|    防御力              35 |"
        print "+---------------------------+"

        hp = 600
        mp = 200
        ak = 80
        df = 35

    else:
        over("你选取的机体不存在")


def gundam():

    global NUMBER  
    print "  Zeastion >>> 恭喜你 \033[5;31;47m%s\033[0m ，你获得了Gundam的驾驶权" % name 
    print    
    time.sleep(0.5)
    print "  Zeastion >>> 现在，来选择属于你的高达吧\n\n"
    time.sleep(0.8)
    print "    *** 机体库开启中 ***\n"
    time.sleep(1.5)


    # 查询高达机体信息
    
    
    while True:

        print "+---------------------------"
        print "| 1- GAT-X105 Strike Gundam"
        time.sleep(0.3)
        print "----------------------------"
        print "| 2- MSZ-008 ZII GUNDAM ZII"
        time.sleep(0.3)
        print "----------------------------"
        print "| 3- ZM-S08G ZOLO"
        time.sleep(0.3)
        print "----------------------------"
        print "| 4- GN-001 Gundam Exia"
        print "+---------------------------\n\n"


       # global NUMBER
        NUMBER = raw_input("输入你想查看的高达编号，了解该机体的详细信息[1-4]: ")
        print  
        print "   \033[5;31;47m%s\033[0m 号机体数据载入中 ...\n" % NUMBER
        time.sleep(1)

        information()
        
        again = raw_input("\n继续查看其他型号吗？(y/n)")
        
        if again == "y":
            print
            print "好的 请稍侯 ..."
            time.sleep(0.5)
            print
        elif again == "n":
            print
            print "好的 请稍侯 ...\n"
            time.sleep(0.5)
            getgun()
        else:
            over("型号查看步骤出错")
    
   # 选择所要驾驶的高达

def getgun():

    global GOLD
    global NUMBER
    print "  Zeastion >>> 现在你已经掌握这几台高达的数据了吧\n"
    print "  Zeastion >>> 那么 想要哪一台？\n"
       
    NUMBER = raw_input("请输入选择的机体编号[1-4]：")
    print
    print "高达信息更新 绑定专属驾驶员 ...\n\n"
    time.sleep(1.2)
    information()
    print "|    驾驶员          %s |" % name
    print "+---------------------------+\n"
    time.sleep(0.5)
    raw_input("绑定完毕 敲击回车确定")

    print
    print "  Zeastion >>> 好了 \033[5;31;47m%s\033[0m 接下来需要给你的高达配备各种装备\n               具体事宜武备店老板 Jack 会给你做详细介绍 再会" % name
    print
    print "  1- \"多谢老Z！\""
    print "  2- \"真是受够你了 快走吧\""
    print
    ans1 = raw_input("Type[1 or 2]: ")
    
    if ans1 == "1":
        print "\n  Zeastion >>> 不用客气 这里有你的启动资金 可以去购买高级装备 后会有期！\n"
        raw_input("按下回车接过钱袋 >>> ")
        print "\n   *** 得到金币2000 ***"
        time.sleep(0.5) 
        GOLD = 2000
        print "\n账户余额：%d\n" % GOLD

        print "\n前往武备店 ..."
        time.sleep(1)
        equip()
        
    elif ans1 == "2":
        print "\n  Zeastion >>> 呵呵\n"
        print "\t系统提示：获得新手金币奖励 1000\n"
        GOLD = 1000
        print "账户余额：%d\n" % GOLD
        raw_input("按下回车离开")
        print "\n前往武备店 ..."
        print
        time.sleep(1)
        equip()

    else:
        over("闹哪样？")
   

def pwd_input():
    sys.stdout.write ('初始化GundamOS 请输入启动密码: ')
    pwd = []    
    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    while True:
        #newChar = msvcrt.getch()
        try :
            tty.setraw( fd )
            newChar = sys.stdin.read( 1 )
        finally :
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)

        if newChar in '\r\n':
            break
        #elif newChar in '\b': 
        elif newChar in '\x7f': 
            if pwd:
                del pwd[-1] 
                sys.stdout.write('\b \b')
                sys.stdout.flush()
                #msvcrt.putch('\b')
        else:
            pwd.append(newChar)
            sys.stdout.write('*') 
    pwd=''.join(pwd)
    print
    return pwd



def start():
    
    print "\n\t**************************"
    print "\t* 欢迎来到GUNDAM设计工厂 *"
    print "\t**************************"

    print "  \n  既然你来到这里 说明你准备好了为和平而战 \n  下面让我们的首席教官Zeastion.Lee为你做具体说明 \n\n  Zeastion >>> 你好，恭喜你成为Gundam预备驾驶员, 接下来我需要记录你的相关信息"
    global GOLD
    global name 
    name = raw_input("  Zeastion >>> 姓名？")
    age = raw_input("  Zeastion >>> 年龄？")
    nationality = raw_input("  Zeastion >>> 来自哪个国家？")

    raw_input("  Zeastion >>> 好的 我已经做好了记录 请你确认后输入启动密码\n               这个密码在这里设定 以后启动你的 GundamOS 时需要输入比对，请牢记！")

    print "\n  +---------"
    print "  | NAME    %s   " % name
    print "  | AGE     %s   " % age
    print "  | NATI    %s   " % nationality
    print "  +---------\n"
    print "驾驶员信息记录中 ...\n"
    time.sleep(0.5)
    
    global PASSWORD
    PASSWORD = pwd_input()
    
    print "\n写入启动密码 ...\n"
    time.sleep(0.5)
    print "绑定身份信息：\n"
    time.sleep(0.5)
    print "  指纹 --> OK\n"
    time.sleep(0.5)
    print "  血型 --> OK\n"
    time.sleep(0.5)
    print "  密码 --> OK\n"
    time.sleep(1)
    print "记录完毕！\n"    
    raw_input("按下回车确认以上信息\n\n")

    gundam()

    
    
start()
